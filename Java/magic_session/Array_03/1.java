/*
Print count of digit
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}


		System.out.println("Count of digit");

		for(int i=0;i<len;i++){
			
			int count=0;

			while(arr[i]>0){
				arr[i]=arr[i]/10;
				count++;
			}

				System.out.println(+count);
		}


	}
}	
