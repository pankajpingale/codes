//print square of even digit of given No.

class Number{
	public static void main(String[] args){

		int num=942111423;
		int rem=0;
		while(num!=0){
			rem=num%10;
			if(rem%2==0){
				System.out.println(+(rem*rem));
			}
			num/=10;
		}
	}
}
