/*
Units <= 100 : price per unit is 1
Units >=100 : price per units is 2
input = 50
o/p = 50
input = 105
output = 110
*/
class Bill{
	public static void main(String[] args){

		int unit=110;

		if(unit>=100)
			System.out.println(unit+" is Bill");
		else
			System.out.println(((unit-100)*2+100)+"is Bill");
	}
}
