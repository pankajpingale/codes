/*
Integer is input
Print fizz if it is divisible by 3
buzz if 5
fizz buzz if both
not then "Not divisible by both"
*/
class Number{
	public static void main(String[] args){
	
		int num=15;

		if(num%3==0 && num%5==0)
			System.out.println("fizz-buzz");
		else if(num%3==0)
			System.out.println("fizz");
		else if(num%5==0)
			System.out.println("buzz");
		else 
			System.out.println("Not divisible by both");

	}
}

