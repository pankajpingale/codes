/*
Print element if their digit sum is even
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter elements are:");
		
		for(int i=0;i<len;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int div=0;


		System.out.println("Even sum elements are:");

		for(int i=0;i<len;i++){
			
			int num=arr[i];

			int sum=0;

			while(num>0){
				div=num%10;
				sum=sum+div;
				num=num/10;
			}

			if(sum%2==0){
				System.out.println(arr[i]);
			}
		}


	}
}	
