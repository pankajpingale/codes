class Demo{

	public static void main(String[] args){

		int arr1[]=new int[]{10,20,30};
		
		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1[2]);
		System.out.println(arr1[3]);
		
		char arr2[]={'A','B','C','D'};
		
		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		System.out.println(arr2[2]);
		System.out.println(arr2[3]);
//		System.out.println(arr2[4]);
		
		float arr3[]={10.5f,20.5f};
		
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
//		System.out.println(arr3[2]);
		
		boolean arr4[]={true,false};
		
		System.out.println(arr4[0]);
		System.out.println(arr4[1]);
//		System.out.println(arr4[2]);
	}
}
