import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Length:");
		
		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter Array Element:");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("No. Divisible by 5 are:");
		
		for(int i=0;i<arr.length;i++){

			if(arr[i]%5==0){

				System.out.println(arr[i]);

			}
		}

	}
}

			
