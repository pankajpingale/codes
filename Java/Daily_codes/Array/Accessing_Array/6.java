import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Length:");
		
		int len=Integer.parseInt(br.readLine());

		int arr[]=new int[len];

		System.out.println("Enter Array Element:");

		int count=0;
		int count1=0;

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
			
			if(arr[i]%2==0){
				
				count++;

			}else{
				
				count1++;
			}
		}
		
		System.out.println("count of even no. is "+count);
		System.out.println("count of odd no. is "+count1);
	}
}

			
