class Number{
	public static void main(String[] args){
		
		int num=145;
		int sum=0;
		int realNo=num;

		while(num!=0){
			int rem=num%10;
			int mult=1;
			for(int i=1;i<=rem;i++){
				mult=mult*i;
			}
			sum=sum+mult;
			num/=10;
		}
		if(realNo==sum){
			System.out.println(realNo+" is Strong Number");
		}else{
			System.out.println(realNo+" is not Strong Number");
		}
	}
}

