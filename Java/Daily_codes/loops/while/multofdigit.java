/*
Given an Integer N
Print multiplication of its digit 
input:135
o/p:15
*/

class Number{
	public static void main(String[] args){
		int N=135;
		int mult=1;
		while(N!=0){
			mult=mult*(N%10);
			N=N/10;
		}
		System.out.println(mult);
	}
}

