/*
Integer N as input
Print multiples of 4 till N
*/

class Number{
        public static void main(String[] args){

                int N=22;
                int i=4;
                while(i<=N){
                        System.out.println(i);
                        i=i+4;
		}
	}
}  
