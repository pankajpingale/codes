import java.io.*;
class myCompDemo{

	static int mycompare(String str1,String str2){

		char ch1[]=str1.toCharArray();
		char ch2[]=str2.toCharArray();
		
		int len=0;

		if(ch1.length!=ch2.length){
			
			return ch1.length-ch2.length;
		}

		for(int i=0;i<ch1.length;i++){
			if(ch1[i]!=ch2[i]){

				return ch1[i]-ch2[i];

			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter First String");
		String str1=br.readLine();
		
		System.out.println("Enter Second String");
		String str2=br.readLine();
		
		int diff=mycompare(str1,str2);

		System.out.println(diff);
	}
}

