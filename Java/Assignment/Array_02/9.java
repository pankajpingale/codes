/*
 merge array
*/
import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter length of array:");

		int len=Integer.parseInt(br.readLine());

		int arr1[]=new int[len];
	
		System.out.println("Enter elements in first array:");
		
		for(int i=0;i<len;i++){

			arr1[i]=Integer.parseInt(br.readLine());
		}

		int arr2[]=new int[len];
		
		System.out.println("Enter elements in second array:");
		
		for(int i=0;i<len;i++){

			arr2[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Merge array are:");

		int arr3[]=new int[len*len];

		for(int i=0;i<len;i++){

			arr3[i]=arr1[i];
		}

		for(int i=0;i<len;i++){

			arr3[len+i]=arr2[i];
		}

		for(int i=0;i<len+len;i++){

			System.out.println(arr3[i]);
		}
	
		
	}
}	
