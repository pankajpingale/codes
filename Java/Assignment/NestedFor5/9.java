//Addition of Factorial of each digit from number.
import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter number:");

		int num=Integer.parseInt(br.readLine());

		int sum=0;

		while(num!=0){

			int div=num%10;

			int fact=1;

			for(int i=div;i>=1;i--){

				fact=fact*i;
			}

			sum=sum+fact;

			num=num/10;
		}
		System.out.println("Sum="+sum);
	}
}

		
