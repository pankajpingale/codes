/*
Checks a No. from 0 to 5 & Print its spelling if the No. is greater than 5
Print No. is greater than 5 
*/

class Number{

	public static void main(String[] args){

		int num=-8;
		
		if(num>0&&num<6){
			if(num==0){
				System.out.println("Zero");
		}else if(num==1){
				System.out.println("One");
		}else if(num==2){
				System.out.println("Two");
		}else if(num==3){
				System.out.println("Three");
		}else if(num==4){
				System.out.println("Four");
		}else{
				System.out.println("Five");
		}
		}else if(num>5){
				System.out.println(num+" is greater than 5");
		}else{
				System.out.println(num+" is less than 0");
		}
	}
}


