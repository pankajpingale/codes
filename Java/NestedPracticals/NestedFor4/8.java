/*
10 
I H 
7 6 5 
D C B A
*/
class Pattern{
	public static void main(String[] args){

		int N=4;
		int Num1=(N*N+N)/2;

		for(int i=1;i<=N;i++){
		
			for(int j=1;j<=i;j++){
		
				if(i%2==0){
		
					System.out.print((char)(Num1+64) + " ");
					Num1--;
	
				}else{
					System.out.print(Num1-- + " ");
		
				}
			}
			System.out.println();
		}
	}
}
