/*
A	B	C	D	
B	C	D	
C	D	
D
*/
class Pattern{
	public static void main(String[] args){

		int N=4;

		for(int i=1;i<=N;i++){
			int Num=64+i;
			for(int j=1;j<=N-i+1;j++){

				System.out.print((char)Num++ + "\t");

			}
			System.out.println();
		}
	}
}
