/*
WAP to swap values of two numbers using a pointer.
(Hint: Use de-referencing of pointers)
Input : x=10
y=20
Op: After swapping
x=20
y=10
*/
#include<stdio.h>
void main(){
	
	int x=10;
	int y=20;
	
	int *ptr1=&x;
	int *ptr2=&y;

	printf("%d\n",*ptr1);
	printf("%d\n",*ptr2);

	int temp;

	temp=*ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;

	printf("%d\n",*ptr1);
	printf("%d\n",*ptr2);
}

