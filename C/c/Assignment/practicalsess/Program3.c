/*
WAP to add two different arrays of the same size
Take array size and array elements from the user
IP : enter 1st array : 10 12 13 15
Ip : enter 2nd array: 1 2 3 4
Op: 11 14 16 19
*/
#include<stdio.h>
void main(){
	
	int size,x;
	
	printf("Enter the size of array:\n");\
	scanf("%d",&size);

	int arr1[size];
	int arr2[size];
	
	printf("Enter the elements in array1:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr1[i]);
	}
	printf("Enter the elements in array2:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr2[i]);
	}

	int arr3[size];
	printf("The addition of arrays is:\n");
	for(int i=0;i<size;i++){
		x=arr1[i]+arr2[i];
		printf("%d\n",x);
	}
}
	
