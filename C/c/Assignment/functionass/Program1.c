//WAP to take value from user in main function and print the value in another function

#include<stdio.h>

void fun(int num,char ch){
	
	printf("Number = %d\n",num);
	printf("Character = %c\n",ch);
}

void main(){

	int num;
	char ch;

	printf("Enter the number:\n");
	scanf("%d",&num);

	printf("Enter the character:\n");
	scanf(" %c",&ch);

	fun(num,ch);
}
