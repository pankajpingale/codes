//WAP to take an Array from user in main function and print the array in another function

#include<stdio.h>
void printArr(int * arr,int size){
	printf("Array elements are:\n");
	for(int i=0;i<size;i++){
		printf("%d\n",*(arr+i));
	}
}
void main(){
	
	int size;

	printf("Enter the size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter the array elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	
	printArr(arr,size);
}
