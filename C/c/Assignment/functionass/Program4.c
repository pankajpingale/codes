//WAP to take an array from the user in another Function and print that array in Main Function. take the array size from the user
#include<stdio.h>

int* printArr(int * arr,int size){
	printf("Enter the array elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	return arr;
}
void main(){
	
	int size;

	printf("Enter the size of arrry:\n");
	scanf("%d",&size);

	int arr[size];

	int *ptr = printArr(arr,size);
	printf("Array elements are:\n");
	for(int i=0;i<size;i++){
		printf("%d\n",*(ptr+i));
	}

}


