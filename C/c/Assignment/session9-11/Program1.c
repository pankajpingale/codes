//WAP that accept string from user and print length of string. use mystrlen().
#include<stdio.h>

int mystrlen(char *src){

	int count=0;
	while(*src!='\0'){
		src++;
		count++;
	}
	return count;
}
void main(){

	char arr[100];
	printf("Enter string:\n");
	gets(arr);

	printf("The length of string is:%d\n",mystrlen(arr));
}
