/*
WAP that accept an aaray on length N from the user and calculate square of all even elements and cubes of all odd elements from that array and replaces the elements respectively with the answer

IP = Length of Array:4
     Enter elements in array: 1 2 3 4
  
     OP = 1 4 27 16
*/
#include<stdio.h>
void main(){

	int arrsize;

	printf("Enter size of array:\n");
	scanf("%d",&arrsize);

	int arr[arrsize];

	printf("Enter array elements:\n");
	for(int i=0;i<arrsize;i++){
		scanf("%d",&arr[i]);
		
		if(arr[i]%2!=0){
			arr[i]=arr[i]*arr[i]*arr[i];
		}else{
			arr[i]=arr[i]*arr[i];
		}
	}
	
	for(int i=0;i<arrsize;i++){
		printf("%d\n",arr[i]);
	}
}


