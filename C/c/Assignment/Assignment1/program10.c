//WAP to check whether the character is vowel or constant.
#include<stdio.h>
void main(){
	char ch;
	printf("Enter the character:\n");
	scanf("%c",&ch);
	if(ch=='A'||ch=='a'||ch=='E'||ch=='e'||ch=='I'||ch=='i'||ch=='O'||ch=='o'||ch=='U'||ch=='u'){
		printf("%c is vowel\n",ch);
	}else{
		printf("%c is constant\n",ch);
	}
}
