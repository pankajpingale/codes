/*
 WAP to print the value and size  of the below variable.Take all the value from user.
 */
#include<stdio.h>
void main(){
	int num;
	char chr;
	float rs;
	double crMoney;

	printf("Enter the number:\n");
	scanf("%d",&num);

	printf("Enter the Character\n");
	scanf(" %c",&chr);

	printf("Enter the Rupees:\n");
	scanf("%f",&rs);

	printf("Enter the Money:\n");
	scanf("%lf",&crMoney);

	printf("Num=%d\n",num);
	printf("Size of Num=%ld\n",sizeof(num));

        printf("Chr= %c\n",chr);
        printf("Size of chr= %ld\n",sizeof(chr));

        printf("rs=%f\n",rs);
        printf("Size of rs=%ld\n",sizeof(rs));

        printf("crMoney=%lf\n",crMoney);
        printf("Size of crMoney=%ld\n",sizeof(crMoney));
}

