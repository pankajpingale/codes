//WAP to take numerical input from the user and find number is divisible by 5 and 11
#include<stdio.h>
void main(){
	int num;
	printf("Enter the number:\n");
	scanf("%d",&num);
	if(num%5==0 && num%11==0){
		printf("%d is divisible by 5 & 11",num);
	}else{
		printf("%d is not  divisible by 5 & 11",num);
	}
}
