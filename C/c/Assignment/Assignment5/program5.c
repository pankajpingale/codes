//WAP to take the number input and print all the factors of that number
#include<stdio.h>
void main(){
	int num;
	printf("Enter the number:\n");
	scanf("%d",&num);
	for(int i=1;i<=num;i++){
		if(num%i==0){
			printf("The factor of %d is %d\n",num,i);
		}
	}
}
