//WAP to calculate the square root of range from 100 to 300
#include<stdio.h>
#include<math.h>
int main(){
	int x,y;
	printf("Enter the starting number of range:\n");
	scanf("%d",&x);
	printf("Enter the ending number of range:\n");
	scanf("%d",&y);
	for(int i=x;i<=y;i++){
		float a=sqrt(i);
		printf("%.2f\n",a);
	}
}
