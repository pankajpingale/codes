/*
 WAP to print the numbers in a given range and their multiplicative inverse.
Suppose x is a number then its multiplicative inverse or reciprocal is 1/x.
The expected output for range 1 - 5
1 = 1
2 = 1/2 i.e 0.5
3 = 1/3 i.e 0.33
4 = 0.25
5 = 0.2
*/
#include<stdio.h>
void main(){
	int x,y;
	double a=0;
	printf("Enter the starting number of range:\n");
	scanf("%d",&x);
	printf("Enter the ending number of range:\n");
	scanf("%d",&y);
	for(int i=x;i<=y;i++){
		a=1.0/i;
		printf("%d=%.2lf\n",i,a);
	}
}
		
