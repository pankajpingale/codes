//WAP to calculate LCM of given two numbers
#include<stdio.h>
void main(){
	int num1,num2,gcd,lcm=1;
	printf("Enter the first number:\n");
	scanf("%d",&num1);
	printf("Enter the second number:\n");
	scanf("%d",&num2);
	for(int i=1;i<=num1 && i<=num2;i++){
		if(num1%i==0 && num2%i==0){
			gcd=i;
		}
	}
	lcm=(num1*num2)/gcd;
	printf("Lcm of %d & %d is %d\n",num1,num2,lcm);
}

