//WAP to print the all even numbers in reverse order and odd numbers in the standard way
#include<stdio.h>
void main(){
	int x,y;
	printf("Enter the starting number of range:\n");
	scanf("%d",&x);
	printf("Enter the ending number of range:\n");
	scanf("%d",&y);
	for(int i=y;i>=x;i--){
		if(i%2==0){
			printf("%d\t",i);
		}
	}printf("\n");
	for(int i=x;i<=y;i++){
		if(i%2!=0){
			printf("%d\t",i);
		}
	}
}
