/*
4 a 3 b
4 a 3 b
4 a 3 b
4 a 3 b
*/
#include<stdio.h>
void main(){
	int rows;
	printf("Enter the no. of rows:\n");
	scanf("%d",&rows);
	for(int i=1;i<=rows;i++){
		int num=rows;
		char ch='a';
		for(int j=1;j<=rows;j++){
			if(j%2==1){
				printf("%d ",num);
				num--;
			}else{
				printf("%c ",ch);
				ch++;
			}
		}
		printf("\n");
	}
}
