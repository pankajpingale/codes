/*
a B c D 
b C d E 
c D e F 
d E f G 
 */
#include<stdio.h>
void main(){
	int rows;
	printf("Enter the no. of rows:\n");
	scanf("%d",&rows);
	for(int i=0;i<rows;i++){
		for(int j=1;j<=rows;j++){
			if(j%2==0){
			   printf("%c ",i+j+64);
		}else{
			printf("%c ",i+j+96);
		}
		}
	printf("\n");
	}
}
