/*
WAP to print different datatype of elements using Void pointer
int , char , float , double
*/
#include<stdio.h>
void main(){

	int a=10;
	char b='A';
	float c=20.5;
	double d=30.5678;

	void *vptr=&a;
	void *bptr=&b;
	void *cptr=&c;
	void *dptr=&d;

	printf("%d\n",*((int*)vptr));
	printf("%c\n",*((char*)bptr));
	printf("%f\n",*((float*)cptr));
	printf("%lf\n",*((double*)dptr));

}
