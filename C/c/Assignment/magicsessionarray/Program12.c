/*
WAP to print addition of both diagonal elements using pointer without repeating the
middle element
*/
#include<stdio.h>
void main(){
	
	int arr[3][3];

	int *ptr=arr[0];
	int sum=0;

	printf("Enter Array Elements:\n");
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			scanf("%d",&arr[i][j]);
			if((i+j)%2==0){
			sum=sum+*(ptr+(i+j));
			}
		}

	}printf("sum=%d",sum);
}

	
