/*
Create a 2D array and take the input of elements from the user & Print using a pointer.
Draw a correct diagram and Write the output.
*/
#include<stdio.h>
void main(){

	int rows,colm;

	printf("Enter the no. of rows\n");
	scanf("%d",&rows);

	printf("Enter the no. of colm\n");
	scanf("%d",&colm);

	int arr[rows][colm];

	printf("Enter Array Elements:\n");
	for(int i=0;i<rows;i++){
		for(int j=0;j<colm;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	int *ptr=arr[0];

	printf("Array Elements are:\n");
	for(int i=0;i<rows;i++){
		for(int j=0;j<colm;j++){
			printf("%d ",*(ptr+(i+j)));
		}
		printf("\n");
	}
}
