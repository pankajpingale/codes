/*
WAP to make an array of the table of Given Number
Print the array using a pointer
Input: 2.
Output : 2 4 6 8 10 12 14 16 18 20.
*/
#include<stdio.h>
void main(){

	int num,table;

	printf("Enter the number:\n");
	scanf("%d",&num);

	int arr[10];
	for(int i=0;i<10;i++){
		table=num*(i+1);
		arr[i]=table;
	}
	printf("Array is:\n");
	for(int i=0;i<10;i++){
		printf("%d\n",*(arr+i));
	}
}
