/*
E D C B A
E D C B
E D C
E D
E
*/
#include<stdio.h>
void main(){
	int rows;
	printf("Enter the no. of rows:\n");
	scanf("%d",&rows);
	for(int i=1;i<=rows;i++){
		for(int j=rows;j>=i;j--){
			printf("%c ",j+64);
		}
		printf("\n");
	}
}
