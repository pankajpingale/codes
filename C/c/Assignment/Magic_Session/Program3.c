/*
WAP to print all the composite numbers between a given range
Input : Enter Start 1
Enter End 15
Output:
4 6 8 9 10 12 14 15
Input : Enter Start 31
Enter End 35
Output:
32 33 34 35
*/
#include<stdio.h>
void main(){
	int x,y,count=0;
	printf("Enter the starting num of range:\n");
	scanf("%d",&x);
	printf("Enter the ending num of range:\n");
	scanf("%d",&y);
	for(int i=x;i<=y;i++){
		for(int j=1;j<=i;j++){
			if(i%j==0){
				count++;
			}
		}
		if(count>2){
			printf("%d ",i);}
		count=0;
	}
}
