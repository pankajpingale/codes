/*
Write a function which returns the reverse number of the given 
number.
*/
#include<stdio.h>
int ret(int num){
	
	int rem;
	int rev=0;

	while(num!=0){
		rem=num%10;
		rev=rev*10+rem;
		num=num/10;
	}
	return rev;
}

void main(){

	int num;

	printf("Enter the number:\n");
	scanf("%d",&num);

	printf("%d\n",ret(num));
}
