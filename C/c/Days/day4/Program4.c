/*
Write a function which checks the given number is prime number 
or not.
*/
#include<stdio.h>
void primeNo(int num){
	
	int count=0;

	for(int i=1;i<=num;i++){
		if(num%i==0){
			count++;
		}
	}
	if(count<3)
		printf("%d is prime number\n",num);
	else
		printf("%d is not prime number\n",num);
		
}
void main(){
	
	int num;
	
	printf("Enter the number:\n");
	scanf("%d",&num);

	primeNo(num);
}

