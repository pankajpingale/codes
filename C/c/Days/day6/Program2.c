/*
WAP to use predefined length function and your own length function for array.(get
array from user).
*/
#include<stdio.h>
int mystrlen(char *carr){

	int count=0;
	while(*carr!=0){
		count++;
		*carr++;
	}
	return count;
}
void main(){


	char carr[10]={'P','a','n','k','a','j'};
	char *str="Virat Kohli";

	printf("%d\n",mystrlen(carr));
	printf("%d\n",mystrlen(str));
}
