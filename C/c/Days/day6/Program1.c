/*
WAP to initilise a character array or String in a multiple way and print (from user and
hardcoaded using for loop and get()).
*/
#include<stdio.h>
void main(){

	char carr[]={'c','o','r','e','2','w','e','b'};
	char name[10];

	printf("Enter String:\n");
	gets(name);

	for(int i=0;i<8;i++){
		printf("%c",carr[i]);
	}
	printf("\n");
	for(int i=0;i<8;i++){
		printf("%c",name[i]);
	}
}
