/*
WAP to create 1-D array of length n from user and also take value 
from user and print whole 1-D array.
*/
#include<stdio.h>
void main(){

	int size;

	printf("Enter the Size of array:\n");
	scanf("%d",&size);

	int iarr[size];

	printf("Enter Array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&iarr[i]);
	}

	printf("Array Elements are:\n");
	for(int i=0;i<size;i++){
		printf("%d\n",iarr[i]);
	}
}
