/*
WAP to create 2-D array of size given from user and assign that 
array to another array of same size and print the second array.
*/
#include<stdio.h>
void main(){

	int rows,colms;

	printf("Enter the no. of rows & colms:\n");
	scanf("%d%d",&rows,&colms);

	int iarr1[rows][colms];

	printf("Enter array ELments:\n");
	for(int i=0;i<rows;i++){
		for(int j=0;j<colms;j++){
		scanf("%d",&iarr1[i][j]);
		}
	}
	int iarr2[rows][colms];
	
	for(int i=0;i<rows;i++){
		for(int j=0;j<colms;j++){
			iarr2[i][j]=iarr1[i][j];
		}
	}

	printf("Array ELments second array are :\n");
	for(int i=0;i<rows;i++){
		for(int j=0;j<colms;j++){
		printf("%d\n",iarr2[i][j]);
		}
	}

}
