/*
 WAP to compare two arrays by with length and elements given by 
user if array are equal then print “Equal” otherwise print “Not 
Equal”.
*/
#include<stdio.h>
void main(){

	int size;
	int flag=0;

	printf("Enter the size of array:\n");
      	scanf("%d",&size);

	int arr1[size];
	int arr2[size];

	printf("Enter the element of arr1:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr1[i]);
	}
 	
	printf("Enter the element of arr2:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr2[i]);
	}

	for(int i=0;i<size;i++){
		if(arr1[i]==arr2[i]){
			flag=1;
		}else{
			flag=0;
		}
	}
	if(flag==0)
		printf("Arrays are Equal\n");
	else
		printf("Arrays are not Equal\n");
}
