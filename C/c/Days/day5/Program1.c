/*
WAP to take size, integer array element from user in main function and print all even 
element in evenArr() function(passing Array to function).
*/
#include<stdio.h>
void evenArr(int arr[],int size){
	printf("Even elements in array are:\n");
	for(int i=0;i<size;i++){
		if(arr[i]%2==0){
			printf("%d\n",arr[i]);
		}
	}
}
void main(){

	int size;

	printf("Enter the size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	evenArr(arr,size);
}
