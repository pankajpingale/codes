//WAP for concept of passing function to function.
#include<stdio.h>
void mult(int x,int y){
	printf("%d\n",x*y);
}
void fun(int x,int y,void(*ptr)(int,int)){
	ptr(x,y);
}
void main(){
	fun(10,20,mult);
}
