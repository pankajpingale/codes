//WAP which shows the concept of Pointer to an array.
#include<stdio.h>
void main(){

	int arr1[3]={1,2,3};
	int arr2[3]={4,5,6};

	int (*ptr1)[3]=&arr1;
	int (*ptr2)[3]=&arr2;

	printf("%d\n",**ptr1);
	printf("%d\n",**ptr2);
}



