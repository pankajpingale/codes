/*
WAP to create array of size n given by user and take the value from 
user sort the array in ascending order and print the sorted array.
*/
#include<stdio.h>
void main(){

	int size,temp;

	printf("Enter the size of array:\n");
	scanf("%d",&size);

	int arr[size];
	
	printf("Enter the elements of array:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	for(int i=0;i<size-1;i++){
		for(int j=i+1;j<size;j++){
			if(arr[i]>arr[j]){
			temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			}
		}
	}
	printf("Array in scending order are:\n");
	for(int i=0;i<size;i++){
		printf("%d\n",arr[i]);
	}
}

