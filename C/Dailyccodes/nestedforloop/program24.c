/*
      A
    A B
  A B C
A B C D
*/
#include<stdio.h>
void main(){
	int rows,num;
	printf("Enter the no. of rows:\n");
	scanf("%d",&rows);
	for(int i=1;i<=rows;i++){
		num='A';
		for(int j=rows-1;j>=i;j--){
			printf("  ");
		}
		for(int k=1;k<=i;k++){
			printf("%c ",num);
			num++;
		}
		printf("\n");
	}
}
