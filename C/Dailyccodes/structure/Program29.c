#include<stdio.h>

union Employee{

	int empId;
	float sal;
};
void main(){

	union Employee emp1={10,50.60};
	printf("%d\n",emp1.empId);
	printf("%f\n",emp1.sal);

	union Employee emp2;
	emp2.empId = 15;
	printf("%d\n",emp2.empId);

	emp2.sal=70.65;
	printf("%f\n",emp2.sal);
}

