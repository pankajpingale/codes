#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Society{

	char sName[20];
	int wings;
	float avgRent;
};
void main(){

	struct Society *ptr=(struct Society*)malloc(sizeof(struct Society));

	strcpy((*ptr).sName,"Sunshine");
	ptr->wings=8;
	ptr->avgRent=10000.50;

	printf("%s\n",(*ptr).sName);
	printf("%d\n",ptr->wings);
	printf("%f\n",ptr->avgRent);
}
