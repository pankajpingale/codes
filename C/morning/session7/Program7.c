/*
          1
       4  7  4
    7 10 13 10 7
10 13 16 19 16 13 10  
*/
#include<stdio.h>
void main(){

	int rows;
	
	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;

	for(int i=1;i<=rows;i++){
		for(int j=rows;j>i;j--){
			printf(" \t");
		}
		for(int k=1;k<=2*i-1;k++){
			if(k<i){
				printf("%d\t",num);
			num+=3;
			}
			else{
				printf("%d\t",num);
			num-=3;
			}
		}
		num+=6;
	printf("\n");
	}
}


