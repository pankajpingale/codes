/*
        4
      3 6 3
    2 4 6 4 2
  1 2 3 4 3 2 1
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=rows;
	int l=rows;
	for(int i=1;i<=rows;i++){
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=2*i-1;k++){
			if(k<i){
				printf("%d ",num);
				num=num+l;
			}else{
				printf("%d ",num);
				num=num-l;
			}
		}
		num=num+l-1;
		l--;
	printf("\n");
	}
}
