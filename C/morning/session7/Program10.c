/*
          A
	b a b
      C E G E C
    d c b a b c d 
  E G I K M K I G E
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		int num=i;
		for(int k=1;k<=2*i-1;k++){
			if(i%2!=0){
				if(k<i){
					printf("%c ",num+64);
					num+=2;
				}else{
					printf("%c ",num+64);
					num-=2;
				}
			}else{
				if(k<i){
					printf("%c ",num+96);
					num--;
				}else{
					printf("%c ",num+96);
					num++;
				}
			}
		}
		printf("\n");
	}
}
					
