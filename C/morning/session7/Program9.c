/*
            5
	  5 6 5
	5 4 3 4 5
      5 6 7 8 7 6 5
    5 4 3 2 1 2 3 4 5
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		int num=rows;
		for(int k=1;k<=2*i-1;k++){
			if(i%2!=0){
				if(k<i){
					printf("%d ",num);
					num--;
				}else{
					printf("%d ",num);
					num++;
				}
			}else{
				if(k<i){
					printf("%d ",num);
					num++;
				}else{
					printf("%d ",num);
					num--;
				}
			}
		}
		printf("\n");
	}
}
					
