/*
      D
    c D c
  B c D c B
a B c D c B a
*/
#include<stdio.h>
void main(){

	int rows;
	
	printf("Enter rows:\n");
	scanf("%d",&rows);
	
	
	for(int i=1;i<=rows;i++){

		int num = 1;
		int x = 1;
		for(int j=rows;j>i;j--){
			printf("  ");
			num++;
			x++;
		}
		for(int k=1;k<=2*i-1;k++){
			
			if(k%2 == 1 ){
				if(x<rows){
					printf("%c ",num+96);
					num++;
				}else{
					printf("%c ",num+96);
					num--;
				}
			}else{
				if(x<rows){
					printf("%c ",num+64);
					num++;
				}else{
					printf("%c ",num+64);
					num--;
				}
			}
		x++;
		num++;
		}
	printf("\n");
	}
}


