/*
       1
     1 2 3
   1 2 3 4 5
 1 2 3 4 5 6 7
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int num=1;
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=2*i-1;k++){
			printf("%d ",num);
			num++;
		}
		printf("\n");
	}
}
