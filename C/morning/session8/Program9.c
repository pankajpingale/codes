/*
A0 B1 C2 D3 E4 F5 G6
   H2 I3 J4 K5 L6
      M4 N5 O6
         P6
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	char ch='A';
	int l=0;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf(" \t");
		}
		int num=l;
		for(int k=1;k<=rows*2-2*i+1;k++){
				printf("%c%d\t",ch,num);
				num++;
				ch++;
			}
		l+=2;
		printf("\n");
	}
}
