/*
D C B A B C D
  c b a b c
    B A B
      a
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=rows;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=1;k<=rows*2-2*i+1;k++){
			if(k<=rows-i){
				if(i%2!=0){
			 		printf("%c ",num+64);
					num--;
				}else{
					printf("%c ",num+96);
					num--;
				}
			}else{
				if(i%2!=0){
					printf("%c ",num+64);
					num++;
				}else{
					printf("%c ",num+96);
					num++;
				}
			}
		}num-=2;
		printf("\n");
	}

}
