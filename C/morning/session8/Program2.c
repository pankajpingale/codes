/*
1 2 3 4 5
  6 7 8 
    9
*/
#include<stdio.h>
void main(){

	int rows;
	
	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf(" \t");
		}
		for(int k=1;k<=rows*2-2*i+1;k++){
			printf("%d\t",num);
			num++;
		}
	printf("\n");
	}
}


