/*
7 6 5 4 3 2 1
  5 4 3 2 1
    3 2 1
      1
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		int num=rows*2-2*i+1;
		for(int k=1;k<=rows*2-2*i+1;k++){
				printf("%d ",num);
				num--;
			}
		printf("\n");
	}
}
