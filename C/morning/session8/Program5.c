/*
d d d d d d d 
  c c c c c 
    b b b 
      a
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=rows;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=1;k<=rows*2-2*i+1;k++){
			printf("%c ",num+96);
		}
		num--;
		printf("\n");
	}
}
