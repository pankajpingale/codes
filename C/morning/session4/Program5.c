/*
4 3 2 1
3 2 1 
2 1
1
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=0;i<rows;i++){
		for(int j=rows;j>i;j--){
			printf("%d ",j-i);
		}
		printf("\n");
	}
}
