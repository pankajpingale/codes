/*
4 3 2 1
C B A
2 1
A
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter the rows:\n");
	scanf("%d",&rows);

	for(int i=0;i<rows;i++){
		for(int j=rows;j>i;j--){
			if(i%2==0){
				printf("%d ",j-i);
			}else{
				printf("%c ",j-i+64);
		}
	}
	printf("\n");
	}
}

