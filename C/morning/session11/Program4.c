/*
      1
    2 1 2
  3 2 1 2 3
4 3 2 1 2 3 4
  3 2 1 2 3
    2 1 2
      1
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
			cols=2*i-1;
			num=cols+1-i;
		}else{
			space=i-rows;
			cols=cols-2;
			num=cols+1-(cols+1)/2;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
	for(int k=1;k<=cols;k++){
		printf("%d ",num);
		if(k<(cols+1)/2){
			num--;
		}else{
			num++;
		}
	}
	printf("\n");
	}
}

