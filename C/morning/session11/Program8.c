/*
      D 
    D C D
  D C B C D
D C B A B C D
  D C B C D
    D C D
      D
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
			cols=2*i-1;
		}else{
			space=i-rows;
			cols=cols-2;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
	int num=rows;
	for(int k=1;k<=cols;k++){
		printf("%c ",num+64);
		if(k<(cols+1)/2){
			num--;
		}else{
			num++;
		}
	}
	printf("\n");
	}
}

