/*
       1
     1 2 1
   1 2 3 2 1
 1 2 3 4 3 2 1
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int num=1;
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=2*i-1;k++){
			if(k<i){
			 printf("%d ",num);
			num++;
		}else{
			printf("%d ",num);
			num--;
		}
	}
	printf("\n");
        }
}
