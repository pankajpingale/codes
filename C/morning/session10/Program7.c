/*i
        1
    1 2 3
1 2 3 4 5
    1 2 3
        1
*/
#include<stdio.h>
void main(){

	int rows,space,cols;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<2*rows;i++){
		if(i<=rows){
			space=2*rows-2*i;
			cols=2*i-1;
		}else{
			space=2*i-2*rows;
			cols-=2;
		}
	int num=1;

	for(int j=1;j<=space;j++){
		printf("  ");
	}
	for(int k=1;k<=cols;k++){
		printf("%d ",num);
		num++;
	}
	printf("\n");
	}
}
