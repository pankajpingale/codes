/*
      1
    2 1
  3 2 1
4 3 2 1
  3 2 1
    2 1 
      1
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
		}else{
			space=i-rows;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
		if(i<=rows){
			cols=i;
		}else{
			cols=2*rows-i;
		}
	int num=cols;
	for(int k=1;k<=cols;k++){
		printf("%d ",num);
		num--;
	}
	printf("\n");
	}
}
 
