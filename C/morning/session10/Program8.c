/*
      D
    C D
  B C D
A B C D
  B C D
    C D
      D
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=rows+1;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
		}else{
			space=i-rows;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
		if(i<=rows){
			cols=i;
			num--;
		}else{
			cols=2*rows-i;
			num++;
		}
	int temp=num;
	for(int k=1;k<=cols;k++){
		printf("%c ",temp+64);
		temp++;
	}
	printf("\n");
	}
}
 
