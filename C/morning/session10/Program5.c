/*
      3
    2 3
  1 2 3
0 1 2 3
  1 2 3
    2 3 
      3
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=rows;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
		}else{
			space=i-rows;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
		if(i<=rows){
			cols=i;
			num--;
		}else{
			cols=2*rows-i;
			num++;
		}
	int temp=num;
	for(int k=1;k<=cols;k++){
		printf("%d ",temp);
		temp++;
	}
	printf("\n");
	}
}
 
