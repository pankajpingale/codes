/*
       1
     2 4
  3  6 9 
4 8 12 16
  3  6 9
     2 4
       1
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=-1;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
		}else{
			space=i-rows;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
		if(i<=rows){
			cols=i;
			num=i;
		}else{
			cols=2*rows-i;
			num=cols;
		}
	int temp=num;
	for(int k=1;k<=cols;k++){
		printf("%d ",temp);
		temp+=cols;
	}
	printf("\n");
	}
}
 
