/*
        1
    3 2 1
5 4 3 2 1
    3 2 1
        1
*/
#include<stdio.h>
void main(){

	int rows,space,cols;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<2*rows;i++){
		if(i<=rows){
			space=2*rows-2*i;
			cols=2*i-1;
		}else{
			space=2*i-2*rows;
			cols-=2;
		}
	int num=cols;

	for(int j=1;j<=space;j++){
		printf("  ");
	}
	for(int k=1;k<=cols;k++){
		printf("%d ",num);
		num--;
	}
	printf("\n");
	}
}
