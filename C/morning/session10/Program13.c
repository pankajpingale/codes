/*
       A
     a b
   B C D
 d e f g
   G H I
     i j
       J
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
		}else{
			space=i-rows;
		}
	for(int j=1;j<=space;j++){
		printf("  ");
	}
		if(i<=rows){
			cols=i;
		}else{
			cols=2*rows-i;
		}
	for(int k=1;k<=cols;k++){
		if(i%2!=0){
			printf("%c ",num+64);
			num++;
		}else{
			printf("%c ",num+96);
			num++;
		}
	}
	num--;
	printf("\n");
	}
}

