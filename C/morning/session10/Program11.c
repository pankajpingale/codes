/*
          1
       1  4
     4 7  10
 10 13 16 19
    19 22 25
       25 28
          28
*/
#include<stdio.h>
void main(){

	int rows,cols,space;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			space=rows-i;
		}else{
			space=i-rows;
		}
	for(int j=1;j<=space;j++){
		printf(" \t");
	}
		if(i<=rows){
			cols=i;
		}else{
			cols=2*rows-i;
		}
	for(int k=1;k<=cols;k++){
		printf("%d\t",num);
		num+=3;
	}
	num-=3;
	printf("\n");
	}
}

