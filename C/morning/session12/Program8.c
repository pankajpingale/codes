/*
       1
     1 b 3
   1 b 3 d 5
 1 b 3 d 5 f 7
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		int num=1;
		for(int k=1;k<=2*i-1;k++){
			if(k%2!=0){
				printf("%d ",num);
				num++;
			}else{
				printf("%c ",num+96);
				num++;
			}
		}
	printf("\n");
	}
}
