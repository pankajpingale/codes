/*
D C B A
  e f g
    F E
      g
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=rows;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		int temp=num;
		for(int k=rows;k>=i;k--){
			if(i%2!=0){
				printf("%c ",temp+64);
				temp--;
			}else{
				printf("%c ",temp+96);
				temp++;
			}
		}
		num++;
	printf("\n");
	}
}
