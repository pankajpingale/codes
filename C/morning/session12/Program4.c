/*
5 D 4 3 B 1
D 3 B 1
3 B 1
B 1
1
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=rows;i>=1;i--){
		int num=i;
		for(int j=1;j<=i;j++){
			if(num%2!=0){
				printf("%d ",num);
				num--;
			}else{
				printf("%c ",num+64);
				num--;
			}
		}
		printf("\n");
	}
}
