/*
g 6 e 4 c 2 a
  5 d 3 b 1
    c 2 a 
      1
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		int num=rows*2-2*i+1;
		for(int k=1;k<=rows*2-2*i+1;k++){
			if(i%2!=0){
				if(k%2!=0){
					printf("%c ",num+96);
					num--;
				}else{
					printf("%d ",num);
					num--;
				}
			}else{
				if(k%2!=0){
					printf("%d ",num);
					num--;
				}else{
					printf("%c ",num+96);
					num++;
				}
			}
		}
		printf("\n");
	}
}
