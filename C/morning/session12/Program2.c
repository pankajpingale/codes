/*
        1 
      1 b
    1 b 2
  1 b 2 d
1 b 2 d 3
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=i;j<rows;j++){
			printf("  ");
		}
		int num=1;
		char ch='a';
		for(int k=1;k<=i;k++){
			if(k%2!=0){
				printf("%d ",num);
				num++;
			}else{
				ch++;
				printf("%c ",ch);
				ch++;
			}
		}
	printf("\n");
	}
}	
