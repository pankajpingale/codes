/*
D c B a
a B c D
F e D c
b C d E
*/
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	
	int num1=rows;
	int num2=1;
	for(int i=1;i<=rows;i++){
		int temp1=num1;
		int temp2=num2;
		for(int j=1;j<=rows;j++){
			if(i%2!=0){
				if(j%2!=0){
					printf("%c ",temp1+64);
					temp1--;
				}else{
					printf("%c ",temp1+96);
					temp1--;
				}
			}else{
				if(j%2!=0){
					printf("%c ",temp2+96);
					temp2++;
				}else{
					printf("%c ",temp2+64);
					temp2++;
				}
			}
		}
		num1++;
		if(i%2==0){
			num2++;
		}
		printf("\n");
	}
}



