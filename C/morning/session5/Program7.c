/*
        A
      b a
    C E G
  d c b a
E G I K M
*/
#include<stdio.h>
void main(){
	
	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
  
	for(int i=1;i<=rows;i++){
		int num=i;
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=i;k++){
			if(i%2!=0){
		        	printf("%c ",num+64);
			num+=2;
			}
			else{
				printf("%c ",num+96);
				num--;
			}
		}
	printf("\n");
	}
}

