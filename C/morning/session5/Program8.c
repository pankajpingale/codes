/*
      1
    A b
  1 2 3
A b C d
*/
#include<stdio.h>
void main(){
	
	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int num=1;
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=i;k++){
			if(i%2!=0){
		        	printf("%d ",num);
			num++;
			}
			else{
				if(k%2!=0){
				   printf("%c ",num+64);
				}else{
					printf("%c ",num+96);
				}
				num++;
			}
		}
	printf("\n");
	}
}

