/*
      4
    4 3
  4 3 2
4 3 2 1
*/
#include<stdio.h>
void main(){
	 
	int rows;
	
	printf("Enter rows:\n");
	scanf("%d",&rows);
	
	for(int i=1;i<=rows;i++){
		int num=4;
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=i;k++){
			printf("%d ",num);
			num--;
		}
	printf("\n");
	}
}
