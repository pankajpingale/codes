/*
      1
    4 7
10 13 16
*/
#include<stdio.h>
void main(){
	 
	int rows;
	
	printf("Enter rows:\n");
	scanf("%d",&rows);
	
	int num=1;
	for(int i=1;i<=rows;i++){
		for(int j=rows;j>i;j--){
			printf(" \t");
		}
		for(int k=1;k<=i;k++){
			printf("%d\t",num);
			num+=rows;
		}
	printf("\n");
	}
}
