/*
        5
      5 6
    5 4 3
  5 6 7 8
5 4 3 2 1
*/
#include<stdio.h>
void main(){
	
	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int num=rows;
		for(int j=rows;j>i;j--){
			printf("  ");
		}
		for(int k=1;k<=i;k++){
			if(i%2!=0){
		        	printf("%d ",num);
			num--;
			}
			else{
				printf("%d ",num);
				num++;
			}
		}
	printf("\n");
	}
}

