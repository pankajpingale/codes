/*
1 2 3 4
  5 6 7
    8 9
      10
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	 
	for(int i=1;i<=rows;i++){
		int num=1;
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=rows;k>=i;k--){
			printf("%d ",num);
			num++;
		}

		printf("\n");
	
	}
}
