/*
1 2 3 4
  2 3 4
    3 4
      4
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	 
	for(int i=1;i<=rows;i++){
		int num=i;
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=rows;k>=i;k--){
			printf("%d ",num);
			num++;
		}

		printf("\n");
	
	}
}
