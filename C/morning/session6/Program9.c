/*
100 9  64 7
    36 5  16
       3  4
          1
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	 
	int num=(rows*rows+rows)/2;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf(" \t");
		}
		for(int k=rows;k>=i;k--){
			if(num%2==0){
				printf("%d\t",num*num);
			}else{
				printf("%d\t",num);
			}
		num--;
		}

		printf("\n");
	
	}
}
