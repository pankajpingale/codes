/*
4 3 2 1
  3 2 1
    2 1
      1
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	 
	for(int i=rows;i>=1;i--){
		int num=i;
		for(int j=0;j<rows-i;j++){
			printf("  ");
		}
		for(int k=1;k<=i;k++){
			printf("%d ",num);
			num--;
		}

		printf("\n");
	
	}
}
