/*
a B c D
  E f G
    h I
      j
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;
	 
	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=rows;k>=i;k--){
			if(num%2!=0){
				printf("%c ",num+96);
			}else{
				printf("%c ",num+64);
			}
			num++;
		}

		printf("\n");
	
	}
}
