/*
D D D D
  c c c
    B B
      a
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	 
	int num=rows;

	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=rows;k>=i;k--){
			if(i%2!=0){
				printf("%c ",num+64);
			}else{
		  		printf("%c ",num+96);
			}
		}
		num--;


		printf("\n");
	
	}
}
