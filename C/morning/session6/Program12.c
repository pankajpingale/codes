/*
A b C d
  e G i
    K n
      q
 */
#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;
	 
	for(int i=1;i<=rows;i++){
		for(int j=1;j<i;j++){
			printf("  ");
		}
		for(int k=rows;k>=i;k--){
			if(i%2!=0){
				if(k%2==0){
					printf("%c ",num+64);
				}else{
					printf("%c ",num+96);
				}
				num+=i;
			}else{
				if(k%2==0){
					printf("%c ",num+96);
				}else{
					printf("%c ",num+64);
				}
				num+=i;
			}
		}

		printf("\n");
	
	}
}
