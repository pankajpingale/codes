/*
1 
1 4
4 7 11
11 13 16 19
19 22 25 
25 28
28
*/
#include<stdio.h>
void main(){

	int rows,cols;

	printf("Enter rows:\n");
	scanf("%d",&rows);

	int num=1;

	for(int i=1;i<rows*2;i++){
		if(i<=rows)
			cols=i;
		else
			cols=rows*2-i;
	for(int j=1;j<=cols;j++){
		printf("%d ",num);
		num+=3;
	}
	num-=3;
	printf("\n");
	}
}
