/*
1 
3 2 1
5 4 3 2 1
3 2 1
1
*/
#include<stdio.h>
void main(){

	int rows,cols;

	printf("Enter rows:\n");
	scanf("%d",&rows);
	
	int num=-1;
	for(int i=1;i<rows*2;i++){
		if(i<=rows){
			cols=2*i-1;
			num+=2;
		}else{
			cols=cols-2;
			num-=2;
		}
	int temp=num;
	for(int j=1;j<=cols;j++){
		printf("%d ",temp);
		temp--;
	}
	printf("\n");
	}
}
