//odd number cube , as it is
#include<stdio.h>
void main(){

	int sNo,eNo;

	printf("Enter the starting and ending value of range:\n");
	scanf("%d%d",&sNo,&eNo);

	for(int i=sNo;i<=eNo;i++){
		if(i%2==0){
			printf("%d ",i*i*i);
		}else{
			printf("%d ",i);
		}
	}
}
