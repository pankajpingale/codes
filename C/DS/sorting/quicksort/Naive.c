#include<stdio.h>
void quicksort(int arr[],int start,int end){
	if(start<end){
		int pivort=partition(arr,start,end);
		quicksort(arr,start,pivort-1);
		quicksort(arr,pivort+1,end);
	}
}

int partition(int arr[],int start,int end){
	int temp=((end-start)+1);
	int index=0;
	int pivort=end;
	for(int i=start;i<=end;i++){
		if(arr[i]<pivort){
			temp(index++)=arr[i];
		}
	}
	int pos=index;
	temp(index++)=pivort;
	for(int i=start;i<=end;i++){
		if(arr[i]>pivort){
			temp[index++]=arr[i];
		}
	}
	for(int i=start;i<=end;i++){
		arr[i]=temp[i-start];
	}
		return pos;
}

void main(){

	int arr[]={4,6,-1,3,-5,8,5};
	int size=sizeof(arr)/sizeof(int);

	quicksort(arr,0,size-1);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
}
