//Two arrys of same size,make their sum on third array.
#include<stdio.h>
void sum(int *arr1,int *arr2,int *arr3,int size){

	for(int i=0;i<size;i++){
		arr3[i]=arr1[i]+arr2[i];
		printf("%d ",arr3[i]);
	}

}
void main(){

	int arr1[]={1,2,5,7,8};
	int arr2[]={3,4,8,9,6};

	int size=sizeof(arr1)/sizeof(int);

	int arr3[size];

	sum(arr1,arr2,arr3,size);
}
