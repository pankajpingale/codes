#include<stdio.h>
void Insertionsort(int *arr,int size){
	for(int i=1;i<size;i++){
		int val=arr[i];
		int j=i-1;
		for(    ;j>=0&&arr[j]>val;j--){
			arr[j+1]=arr[j];
			}
		arr[j+1]=val;
	}
}
void main(){

	int arr[]={4,5,3,-3,2,-1};
	int size=sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	
	printf("\n");

	Insertionsort(arr,size);
	
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}

	printf("\n");

}
