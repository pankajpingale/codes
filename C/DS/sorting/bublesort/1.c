#include<stdio.h>
void Bublesort(int *arr,int size){

	for(int i=0;i<size-1;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}
}
void main(){

	int arr[]={8,-3,5,3,7,9};
	int size=sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");

	Bublesort(arr,size);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}

