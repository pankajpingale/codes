#include<stdio.h>
void selectionsort(int *arr,int size){
	for(int i=0;i<size-1;i++){
		int minIndex=i;
		for(int j=i+1;j<size;j++){
			if(arr[minIndex]>arr[j]){
				minIndex=j;
			}
		}
		int temp=arr[i];
		arr[i]=arr[minIndex];
		arr[minIndex]=temp;
	}
}
void main(){

	int arr[]={4,5,3,-3,2,-1};
	int size=sizeof(arr)/sizeof(int);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	
	printf("\n");

	selectionsort(arr,size);
	
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}

	printf("\n");

}
