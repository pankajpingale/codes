//Singly Linked List
#include<stdio.h>
#include<stdlib.h>

struct Node{
	int data;
	struct Node *next;
};
void main(){

	struct Node *head=NULL;
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=10;
	newNode->next=NULL;

	head=newNode;
	//Second Node
	newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=20;
	newNode->next=NULL;

	head->next=newNode;
	//Third Node
	newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->data=30;
	newNode->next=NULL;

	head->next->next=newNode;

	struct Node *temp=head;
	while(temp!=NULL){
		printf("%d\n",temp->data);
		temp=temp->next;
	}
	printf("\n");

}
