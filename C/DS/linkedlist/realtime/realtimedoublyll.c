#include<stdio.h>
#include<stdlib.h>

typedef struct Cricket{
	struct Cricket *prev;
	char pName[20];
	int jerNo;
	struct Cricket *next;
}cric;

cric *head=NULL;

cric *createNode(){

	cric *newNode=(cric*)malloc(sizeof(cric));

	newNode->prev=NULL;
	getchar();
	printf("Enter Player Name:\n");
	char ch;
	int i=0;
	while((ch=getchar())!='\n'){
		(*newNode).pName[i]=ch;
		i++;
	}
	printf("Enter jerNo:\n");
	scanf("%d",&newNode->jerNo);
	newNode->next=NULL;
	return newNode;

}
void addNode(){
	
	cric *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		cric *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
void printLL(){

	cric *temp=head;
	while(temp!=NULL){
		printf("%s->",temp->pName);
		printf("%d||",temp->jerNo);
		temp=temp->next;
	}
}
void addFirst(){
	cric *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	int count=0;
	cric *temp=head;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	printf("Count=%d",count);
	return count;
}
int addatPos(int pos){
	
	int count=countNode();

	if(pos<=0||pos>=count+2){
		printf("Wrong Input\n");
	}else if(pos==count+1){
		addLast();
	}else if(pos==1){
		addFirst();
	}else{
		cric *newNode=createNode();
		cric *temp=head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newNode->next=temp->next;
		newNode->prev=temp;
		temp->next->prev=newNode;
		temp->next=newNode;
	}
}
int deleteFirst(){
	int count=countNode();
	if(head==NULL){
		printf("\nList is Empty\n");
		return -1;
	}else if(count==1){
		free(head);
		head=NULL;
	}else{
		head=head->next;
		free(head->prev);
		head->prev=NULL;
	}
	return 0;
}
int deleteLast(){
		int count=countNode();
		if(head==NULL){
			printf("\nList is Empty\n");
			return -1;
		}else if(count==1){
			deleteFirst();
		}else{
		cric *temp=head;
		while(temp->next->next!=NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
		}
		return 0;
}
int deleteatPos(int pos){

	int count=countNode();
		if(pos<=0||pos>count){
			printf("Invalid Position\n");
		}else if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			cric *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addFirst\n");
		printf("4.addlast\n");
		printf("5.countNode\n");
		printf("6.addatPos\n");
		printf("7.deleteFirst\n");
		printf("8.deleteLast\n");
		printf("9.deleteatPos\n");

	int ch;
	int node;

	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:
		       addFirst();
		       break;
		 case 4:
		       addLast();
		       break;
		 case 5:
		       countNode();
		       break;
		 case 6:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			addatPos(pos);
			}
			break;
		 case 7:
			deleteFirst();
			break;
		 case 8:
			deleteLast();
			break;
		 case 9:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			deleteatPos(pos);
			}
			break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}

	getchar();

	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}

