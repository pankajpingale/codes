#include <stdio.h>
#include <stdlib.h>
typedef struct laptop{

	char lName[20];
	float prize;
	struct laptop *next;
}lap;

lap *head = NULL;

lap *createNode(){

	lap *newNode = (lap*)malloc(sizeof(lap));

	getchar();
	printf("Enter laptop name : ");
	
	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
	
		(*newNode).lName[i] = ch;
		i++;
	}

	printf("Enter prize : ");
	scanf("%f", &newNode->prize);
//	getchar();

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct laptop *newNode = createNode();

	if(head == NULL)
		head = newNode;
	else{
	
		lap *temp = head;

		while(temp->next != NULL)
			temp = temp->next;

		temp->next = newNode;
	}
}

void addFirst(){

	lap *newNode = createNode();

	if(head == NULL)
		head = newNode;
	else{
	
		newNode->next = head;
		head = newNode;
	}
}

int countNode(){

	lap *temp = head;
	int count=0;

	while(temp != NULL){
	
		temp = temp->next;
		count++;
	}

	return count;
}

int addAtPos(int pos){

	int count = countNode();

	if(pos <= 0 || pos > count + 1){
	
		printf("Invalid position.\n");
		return -1;
	}else{
	
		if(pos == count + 1)
			addNode();
		else if(pos == 1)
			addFirst();
		else{
		
			lap *newNode = createNode();
			lap *temp = head;

			while(pos-2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next = newNode;
		}

		return 0;
	}
}

void printLL(){


	if(head == NULL){
		printf("LinkedList is empty.\n");
		return;
	}

	lap *temp = head;

	while(temp->next != NULL){
	
		printf("|%s :", temp->lName);
		printf(" %f|->", temp->prize);

		temp = temp->next;
	}
	printf("|%s :", temp->lName);
	printf(" %f|\n", temp->prize);
	return;
}

int deleteFirst(){

	if(head == NULL){
		printf("LinkedList is empty.\n");
		return -1;
	}else{ 
		
		lap *temp = head;
		head = head->next;
		free(temp);

		return 0;
	}
}

int deleteLast(){

	if(head == NULL){
		printf("LinkedList is empty.\n");
		return -1;
	}else{
	
		if(head->next == NULL)
			deleteFirst();
		else{
		
			lap *temp = head;

			while(temp->next->next != NULL)
				temp = temp->next;

			free(temp->next);
			temp->next = NULL;
		}

		return 0;
	}
}

int deleteAtPos(int pos){

	int count = countNode();
	
	if(pos <= 0 || pos > count){
	
		printf("Invalid position.\n");
		return -1;
	}else{
	
		if(pos == count)
			deleteLast();
		else if(pos == 1)
			deleteFirst();
		else{
		
			lap *temp = head;
			lap *temp1 = head;

			while(pos-2){
			
				temp = temp->next;
				pos--;
			}

			temp1 = temp->next;
			temp->next = temp1->next;
			free(temp1);
		}

		return 0;
	}
}

void main(){

	char choice;

	do{
	
		printf("1. addNode.\n");
		printf("2. addFirst.\n");
		printf("3. addAtPos.\n");
		printf("4. countNode.\n");
		printf("5. printLL.\n");
		printf("6. deleteFirst.\n");
		printf("7. deleteLast.\n");
		printf("8. deleteAtPos.\n");

		int ch;
		int pos;

		printf("Enter choice : ");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:

				printf("Enter position : ");
				scanf("%d", &pos);

				addAtPos(pos);

				break;
			case 4:{

				int count=countNode();
				printf("Count=%d\n",count);
			       }
				break;
			case 5:
				printLL();
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
			
				printf("Enter position : ");
				scanf("%d", &pos);
			
				deleteAtPos(pos);
				break;
			default:
				printf("Invalid Choice.\n");
		}

		getchar();

		printf("Do you want to continue ? \n");
		scanf("%c", &choice);

	}while(choice == 'y' || choice == 'Y');
}





















