#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));

	printf("Enter data\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;
}

void addNode(){

	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		node *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
	}
}
void printLL(){

	node *temp=head;
	while(temp->next!=head){
		printf("%d->",temp->data);
		temp=temp->next;
	}
	printf("%d",temp->data);
}
void addFirst(){

	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		node *temp=head;
		
		while(temp->next!=head){
			temp=temp->next;
		}
		
		temp->next=newNode;
		newNode->next=head;
		head=newNode;
	}
}
void addLast(){
	
	addNode();
}
int countNode(){

	if(head==NULL){
		printf("Count=0\n");
	}else{
	node *temp=head;
	int count=0;
	while(temp->next!=head){
		temp=temp->next;
		count++;
	}
	count++;
	printf("count=%d",count);
	return count;
	}
}
int addatPos(int pos){

	node *temp=head;
	int count=countNode();
	if(pos<=0||pos>=count+2){
		printf("Invalid Position\n");
	}else if(pos==count+1){
		addLast();
	}else if(pos==1){
		addFirst();
	}else{
		node *newNode=createNode();
		node *temp=head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newNode->next=temp->next;
		temp->next=newNode;
	}
}
void deleteFirst(){

	if(head==NULL){
		printf("list is Empty\n");
	}else {
		node *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		head=head->next;
		free(temp->next);
		temp->next=head;
	}
}
void deleteLast(){

	if(head==NULL){
		printf("List is Empty\n");
	}else{
		node *temp=head;
		while(temp->next->next!=head){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=head;
	}
}
int deleteatPos(int pos){
	int count=countNode();
	if(pos<=0||pos>count+1){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count+1){
			deleteLast();
		}else{
			node *temp=head;
			node *temp1=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp1=temp->next->next;
			free(temp->next);
			temp->next=temp1;
		}
		return 0;
	}
}

void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addFirst\n");
		printf("4.addlast\n");
		printf("5.countNode\n");
		printf("6.addatPos\n");
		printf("7.deleteFirst\n");
		printf("8.deleteLast\n");
		printf("9.deleteatPos\n");

	int ch;
	int node;

	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:
		       addFirst();
		       break;
		 case 4:
		       addLast();
		       break;
		 case 5:
		       countNode();
		       break;
		 case 6:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			addatPos(pos);
			}
			break;
		case 7:
			deleteFirst();
			break;
		case 8:
		        deleteLast();
		        break;	       
		case 9:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			deleteatPos(pos);
			}
			break;
		default:
		       printf("Enter valid choice:\n");
		       break;
	}

	getchar();

	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}
