#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node* head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));
	newNode->prev=NULL;

	printf("Enter the data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;
	return newNode;
}

void addNode(){

	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
int countNode(){
	node *temp=head;
	int count=0;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	printf("Count=%d\n",count);
	return count;
}

void addFirst(){
	
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int addatPos(int pos){
	int count=countNode();
	if(pos<=0||pos>count+1){
		printf("Wrong Input\n");
	}else if(pos==count+1){
		addLast();
	}else if(pos==1){
		addFirst();
	}else{
		node *newNode=createNode();
		node *temp=head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newNode->next=temp->next;
		newNode->prev=temp;
		temp->next->prev=newNode;
		temp->next=newNode;
	}
}
int deleteFirst(){
	int count=countNode();
	if(head==NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		if(count==1){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			free(head->prev);
			head->prev=NULL;
		}
		return 0;
	}
}
int deleteLast(){
	int count=countNode();
	if(head==NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		if(count==1){
			free(head);
			head=NULL;
		}else{
			node *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
		return 0;
	}

}
int deleteatPos(int pos){
	int count=countNode();
	if(pos<=0||pos>count+1){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count+1){
			deleteLast();
		}else{
			node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
	}
}
	
void printLL(){

	node *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addFirst\n");
		printf("4.addlast\n");
		printf("5.addatPos\n");
		printf("6.deleteatPos\n");
		printf("7.deleteFirst\n");
		printf("8.deleteLast\n");
		printf("9.deleteatPos\n");

	int ch;
	int node;

	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:
		       addFirst();
		       break;
		 case 4:
		       addLast();
		       break;
		 case 5:
		       countNode();
		       break;
		 case 6:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			addatPos(pos);
			}
			break;
		case 7:
			deleteFirst();
			break;
		case 8:
			deleteLast();
			break;
		case 9:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			deleteatPos(pos);
		       }
			break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}

	getchar();

	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}


