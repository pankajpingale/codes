#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}Dem;

Dem *head=NULL;

Dem *createNode(){

	Dem* newNode = (Dem*)malloc(sizeof(Dem));

	getchar();

	printf("Enter the data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
void addNode(){
	Dem* newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Dem* temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void addFirst(){
	Dem *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head=newNode;
	}
}
int addatPos(int pos){
	Dem *newNode=createNode();
	Dem *temp=head;
	while(pos-2){
		temp=temp->next;
		pos--;
	}
	newNode->next=temp->next;
	temp->next=newNode;
}
void addLast(){
	addNode();
}
int countNode(){
	int count=0;
	Dem *temp=head;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
void deleteFirst(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
			printf("Cleared data\n");
		}else{
			Dem *temp=head;
			head=head->next;
			free(temp);
		}
	}
}
void deleteLast(){
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
			printf("Cleared data\n");
		}else{
			Dem *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	}
}
int deleteatPos(int pos){
	int count=countNode();
	if(pos<=0||pos>count+1){
		printf("Invalid Position\n");
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count+1){
			deleteLast();
		}else{
			Dem *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			Dem *temp1=temp->next;
			temp->next=temp->next->next;
			free(temp1);
		}
	}
}
void printLL(){
	Dem *temp=head;
	while(temp!=NULL){
		printf("||%d",temp->data);
		temp=temp->next;
	}
	printf("\n");

}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addatPos\n");
		printf("4.countNode\n");
		printf("5.printLL\n");
		printf("6.addLast\n");
		printf("7.deleteFirst\n");
		printf("8.deleteLast\n");
		printf("9.deleteatPos\n");

	int ch;
	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){

		case 1:{
			int node;
			printf("Enter node\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
				addNode();
			}
		       }
			break;
		case 2:
			addFirst();
			break;
		case 3:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			addatPos(pos);
		       }
			break;
		case 4:{
			int count=countNode();
			printf("Count=%d\n",count);
		       }
		       break;
		case 5:
		       printLL();
		       break;
		case 6:
		       addLast();
		       break;
		case 7:
		       deleteFirst();
		       break;
		case 8:
		       deleteLast();
		       break;
		case 9:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			deleteatPos(pos);
		       }
		       break;
		default:
		       printf("Invalid Choice.\n");
		}

		getchar();

		printf("Do you want to continue ? \n");
		scanf("%c", &choice);

	}while(choice=='y'||choice=='Y');
}




