#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head=NULL;

node *createNode(){

	node *newNode=(node*)malloc(sizeof(node));

	newNode->prev=NULL;

	printf("Enter data\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;
	return newNode;
}
void addNode(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
		newNode->next=head;
		newNode->prev=head;
	}else{
		head->prev->next=newNode;
		newNode->next=head;
		newNode->prev=head->prev;
		head->prev=newNode;
	}
}
void printLL(){

	node *temp=head;
	if(head==NULL){
		printf("Linked list is empty\n");
	}else{
		while(temp->next!=head){
			printf("%d->",temp->data);
			temp=temp->next;
		}
		printf("%d",temp->data);
	}
}
void addFirst(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
		newNode->next=head;
		newNode->prev=head;
	}else{
		newNode->next=head;
		newNode->prev=head->prev;
		head=newNode;
		newNode->next->prev=newNode;
		newNode->prev->next=head;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	if(head==NULL){
		printf("Count=0\n");
	}else{
	int count=0;
	node *temp=head;
	while(temp->next!=head){
		count++;
		temp=temp->next;
	}
	count++;
	printf("count=%d\n",count);
	return count;
	}
}
int addatPos(int pos){
	int count=countNode();
	if(pos<=0||pos>count+1){
		printf("Invalid count\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addLast();
		}else{
			node *newNode=createNode();
			node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
			temp->next=newNode;
			temp->next->prev=newNode;
		}
		return 0;
	}
}
int deleteFirst(){
	if(head==NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			head->next->prev=head->prev;
			head=head->next;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}
int deleteLast(){
	 if(head==NULL){
                printf("Linked list is empty\n");
                return -1;
         }else{
                if(head->next==head){
                        free(head);
                        head=NULL;
		}else{
			node *temp=head->prev->prev;
			free(head->prev);
			temp->next=head;
			head->prev=temp;
		}
		return 0;
	 }
}
int deleteatPos(int pos){
	int count=countNode();

	if(pos<=0 || pos>count){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			node *temp1=head;
			node *temp2=head;

			while(pos-2){
				temp1=temp1->next;
				pos--;
			}
			temp2=temp1->next->next;
			free(temp1->next);
			temp1->next=temp2;
			temp2->prev=temp1;
		}
		return 0;
	}
}

void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addFirst\n");
		printf("4.addlast\n");
		printf("5.countNode\n");
		printf("6.addatPos\n");
		printf("7.deleteFirst\n");
		printf("8.deleteLast\n");
		printf("9.deleteatPos\n");

	int ch;
	int node;

	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:
		       addFirst();
		       break;
		 case 4:
		       addLast();
		       break;
		 case 5:
		       countNode();
		       break;
		 case 6:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			addatPos(pos);
			}
			break;
		case 7:
			deleteFirst();
			break;
		case 8:
			deleteLast();
			break;
		case 9:{
			int pos;
			printf("Enter position:\n");
			scanf("%d",&pos);
			deleteatPos(pos);
			}
			break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}

	getchar();

	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}
