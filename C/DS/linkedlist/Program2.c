#include<stdio.h>
#include<stdlib.h>

typedef struct student{

	int id;
	float ht;
	struct student *next;
}stud;

void main(){

	stud *head=NULL;
	//First Node
	stud *newNode=(stud*)malloc(sizeof(stud));
	newNode->id=1;
	newNode->ht=5.5;
	newNode->next=NULL;

	head=newNode;
	//Second Node
	newNode=(stud*)malloc(sizeof(stud));
	newNode->id=2;
	newNode->ht=6.5;
	newNode->next=NULL;

	head->next=newNode;
	//Third Node
	newNode=(stud*)malloc(sizeof(stud));
	newNode->id=3;
	newNode->ht=7.5;
	newNode->next=NULL;

	head->next->next=newNode;

	stud *temp=head;
	while(temp!=NULL){
		printf("%d ",temp->id);
		printf("%f\n",temp->ht);
		temp=temp->next;
	}
	printf("\n");
}



