// second Last Occurence

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head=NULL;

node *createNode(){
	
	node *newNode=(node*)malloc(sizeof(node));
	
	printf("Enter node\n");
	scanf("%d",&newNode->data);
	
	newNode->next=NULL;
	return newNode;
}

void addNode(){
	
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int secLastOccurence(int num){
	node *temp=head;
	int flag=0,count=0,prev=0,next=0;
	while(temp!=NULL){
		count++;
		if(temp->data==num){
			flag++;
			prev=next;
			next=prev;
		}
		temp=temp->next;
	}
	if(flag==0){
		printf("Data not present");
	}else if(flag==1){
		printf("Data present only one time");
	}else{
		printf("%d",prev);
	}
}
void printLL(){
	
	node *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.secLastOccurence\n");
	int ch;
	int node;
	int num;

	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
	
		case 1:{
			printf("Enter the node:\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
				addNode();
			}
			}
			break;
		case 2:
			printLL();
			break;
		case 3:{
			printf("Enter the number:\n");
			scanf("%d",&num);
			secLastOccurence(num);
		       }
		       break;
		default:
		       printf("Enter valid choice\n");
		       break;
	}
	getchar();

	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');

}

