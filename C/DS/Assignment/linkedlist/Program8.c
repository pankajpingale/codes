//print the minimum intger data from the above nodes
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int num;
	struct Demo *next;
}Dem;

Dem *head=NULL;
void addNode(){

	Dem *newNode=(Dem*)malloc(sizeof(Dem));

	printf("Enter the Number:\n");
	scanf("%d",&newNode->num);

	newNode->next=NULL;

	if(head==NULL){
		head=newNode;
	}else{
		Dem *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int min(){
	Dem *temp=head;
	int num=temp->num;
	while(temp!=NULL){
		if(num>temp->num){
			num=temp->num;
		}
		temp=temp->next;
	}
	return num;
}
void main(){
	addNode();
	addNode();
	addNode();
	int num=min();
	printf("Minimum=%d",num);
}

