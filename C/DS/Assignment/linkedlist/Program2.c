/*
WAP for the Linked list of states in India consisting of its name ,population,budget & literary connect 4 states in the linked list & print the data
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct state{
	char sName[20];
	long popul;
	float budg;
	float lit;
	struct state *next;
}st;

st *head=NULL;

void addNode(){
	 
	st *newNode=(st*)malloc(sizeof(st));

	printf("Enter the State name:\n");
	scanf("%[^\n]",newNode->sName);

	printf("Enter the population:\n");
	scanf("%ld",&newNode->popul);

	printf("Enter the Budget:\n");
	scanf("%f",&newNode->budg);

	printf("Enter the Literacy:\n");
	scanf("%f",&newNode->lit);

	getchar();

	newNode->next=NULL;

	if(head==NULL){
		head=newNode;
	}else{
		st *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printll(){
	st *temp=head;
	while(temp!=NULL){
		printf("State=%s||",temp->sName);
		printf("Population=%ld||",temp->popul);
		printf("Budget=%f||",temp->budg);
		printf("Literacy=%f\n",temp->lit);
		temp=temp->next;
	}
}
void main(){
	addNode();
	addNode();
	addNode();
	addNode();
	printll();
}
