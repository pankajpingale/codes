//WAP for the linked list of festivals in India and count data
#include<stdio.h>
#include<stdlib.h>

typedef struct Festival{
	char fName[20];
	int noGuest;
	struct Festival *next;
}fest;

fest *head=NULL;
void addNode(){

	fest *newNode=(fest*)malloc(sizeof(fest));

	printf("Enter the Festival Name:\n");
	scanf("%[^\n]",newNode->fName);

	printf("Enter the No of Guest:\n");
	scanf("%d",&newNode->noGuest);

	getchar();

	newNode->next=NULL;
	
	if(head==NULL){
		head=newNode;
	}else{
		fest *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void printll(){

	fest *temp=head;
	while(temp!=NULL){
		printf("Festival=%s||",temp->fName);
		printf("No of Guest=%d\n",temp->noGuest);
		temp=temp->next;
	}
}
int count(){
	fest *temp=head;
	int x=0;
	while(temp!=NULL){
		temp=temp->next;
		x++;
	}
	return x;
}
void main(){
	addNode();
	addNode();
	addNode();
	printll();
	int x=count();
	printf("Count=%d\n",x);
}

