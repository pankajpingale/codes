//WAP to check the prime number present in the data from the above codes
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int num;
	struct Demo *next;
}Dem;

Dem *head=NULL;
void addNode(){

	Dem *newNode=(Dem*)malloc(sizeof(Dem));

	printf("Enter the Number:\n");
	scanf("%d",&newNode->num);

	newNode->next=NULL;

	if(head==NULL){
		head=newNode;
	}else{
		Dem *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void prime(){
	Dem *temp=head;
	while(temp!=NULL){
		int flag=0;
		for(int i=2;i<=temp->num;i++){

			if(temp->num%i==0)
				flag++;
			}
			if(flag==1)
				printf("Prime Number=%d\n",temp->num);
			if(temp->num==1)
				printf("%d is not prime not composite\n",temp->num);
				temp=temp->next;
		}
}
void main(){
	addNode();
	addNode();
	addNode();
	prime();
}

