//print the addition of first and last node
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int num;
	struct Demo *next;
}Dem;

Dem *head=NULL;
void addNode(){

	Dem *newNode=(Dem*)malloc(sizeof(Dem));

	printf("Enter the Number:\n");
	scanf("%d",&newNode->num);

	newNode->next=NULL;

	if(head==NULL){
		head=newNode;
	}else{
		Dem *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int add(){
	Dem *temp=head;
	int sum=0;
	int flag=0;
	while(temp!=NULL){
		if(flag==0||temp->next==NULL){
			sum=sum+temp->num;
		}
		flag++;
		temp=temp->next;
	}
	return sum;
}
void main(){
	addNode();
	addNode();
	addNode();
	int sum=add();
	printf("Addition of first and last node=%d",sum);
}

