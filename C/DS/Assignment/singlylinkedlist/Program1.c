/*
WAP that searches for the first occurence of a particular element from a singley linear linked list 
input data elements:10->20->30->40
input:30
output:3
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));
	printf("Enter node:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

void addNode(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int firstOccurence(int num){

	node *temp=head;
	int flag=0;
	int count=0;
	while(temp!=NULL&&flag==0){
		count++;
		if(temp->data==num){
			flag++;
		}
		temp=temp->next;
	}
	if(flag==0){
		printf("Data not found\n");
	}else{
		printf("Position=%d\n",count);
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.firstOccurrence\n");

	int ch;
	int node;
	int num;
	
	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:{
			printf("\nEnter number you want to search:\n");
			scanf("%d",&num);
		        firstOccurence(num);
		 	}
		       break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}
	
	getchar();
	
	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}

