/*
WAP that searches for the occurence of a particular element from a singley linear linked list 
input data elements:10->20->30->40->30->70-30
input:30
output:3
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));
	printf("Enter node:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

void addNode(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int Occurence(int num){

	node *temp=head;
	int count=0;
	while(temp!=NULL){
		if(temp->data==num){
			count++;
		}
		temp=temp->next;
	}
	if(count==0){
		printf("Data not found\n");
	}else{
		printf("Occurence time=%d\n",count);
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.Occurrence\n");

	int ch;
	int node;
	int num;
	
	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:{
			printf("\nEnter number you want to search:\n");
			scanf("%d",&num);
		        Occurence(num);
		 	}
		       break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}
	
	getchar();
	
	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}

