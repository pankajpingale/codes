/*
WAP that accepts a singly linear linked ist from the user.Take number from user and print the data of lenght of that number
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	char str[20];
	struct Node *next;
}node;

node *head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));
	getchar();
	printf("Enter string:\n");
	scanf("%[^\n]",newNode->str);
	newNode->next=NULL;
	return newNode;
}

void addNode(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int printStr(int num){

	node *temp=head;

	while(temp!=NULL){
		int count=0;
		char *ptr=(temp->str);
		while(*ptr!='\0'){
			count++;
			ptr++;
		}
		if(count==num){
			printf("%s\n",temp->str);
		}
		temp=temp->next;
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("%s->",temp->str);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.printStr\n");

	int ch;
	int node;
	int num;
	
	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:{
			printf("Enter number:\n");
			scanf("%d",&num);
		        printStr(num);
			}
		        break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}
	
	getchar();
	
	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}
