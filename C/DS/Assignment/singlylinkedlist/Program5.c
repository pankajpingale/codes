/*
WAP that searches all the palindrome data elemets from a singly linear linked list and print the position of palindrome data
input linked list:112->121->43->321->3223
output:Palindrome at 2
       Palindrome at 4
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));
	printf("Enter node:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

void addNode(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void palindrome(){

	node *temp=head;

	int rem,pos=0;
	
	while(temp!=NULL){
		int sum=0;
		int num=temp->data;
		pos++;
		while(num!=0){
			
			rem=num%10;
			sum=(sum*10)+rem;
			num=num/10;
		}
		if(sum==temp->data){
			printf("Palindrome found at:%d\n",pos);
		}
		temp=temp->next;
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.palindrome\n");

	int ch;
	int node;
	
	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:
		       palindrome();
		       break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}
	
	getchar();
	
	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}

