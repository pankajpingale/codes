/*
WAP that searches the occurence of a particular element from a singly linear linked list
input linked list:10->21->32->43
output linked list:1->3->5->7
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;

node* createNode(){

	node *newNode=(node*)malloc(sizeof(node));
	printf("Enter node:\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

void addNode(){

	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void datadigitSum(){

	node *temp=head;

	while(temp!=NULL){
		int sum=0;
		while(temp->data!=0){

			sum=sum+(temp->data%10);
			temp->data=temp->data/10;
		}
		temp->data=sum;
		temp=temp->next;
	}
}
void printLL(){
	node *temp=head;
	while(temp!=NULL){
		printf("%d->",temp->data);
		temp=temp->next;
	}
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.datadigitSum\n");

	int ch;
	int node;
	int num;
	
	printf("Enter choice:\n");
	scanf("%d",&ch);

	switch(ch){
		case 1:{
			printf("Enter the node :\n");
			scanf("%d",&node);
			for(int i=0;i<node;i++){
			addNode();
			}
		       }
		       break;
		 case 2:
		       printLL();
		       break;
		 case 3:
		       datadigitSum();
		       break;
		 default:
		       printf("Enter valid choice:\n");
		       break;
	}
	
	getchar();
	
	printf("\nDo you want to continue:\n");
	scanf("%c",&choice);
	}
	while(choice=='y'||choice=='Y');
}

