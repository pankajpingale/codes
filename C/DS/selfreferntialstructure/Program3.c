#include<stdio.h>
#include<string.h>
typedef struct Company{
	int compId;
	char cName[20];
	float rev;
	struct Company *next;
}Comp;
void main(){

	Comp obj1,obj2,obj3;
	Comp *head=&obj1;

	head->compId=1;
	strcpy(head->cName,"Google");
	head->rev=500.5;
	head->next=&obj2;

	head->next->compId=2;
	strcpy(head->next->cName,"Microsoft");
	head->next->rev=700.5;
	head->next->next=&obj3;

	head->next->next->compId=3;
	strcpy(head->next->next->cName,"Twitter");
	head->next->next->rev=1000.0;
	head->next->next->next=NULL;

	printf("%d\n",head->compId);
	printf("%s\n",head->cName);
	printf("%f\n",head->rev);
	
	printf("%d\n",head->next->compId);
	printf("%s\n",head->next->cName);
	printf("%f\n",head->next->rev);

	printf("%d\n",head->next->next->compId);
	printf("%s\n",head->next->next->cName);
	printf("%f\n",head->next->next->rev);

}


